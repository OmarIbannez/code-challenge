# Post-mortem

Since I don't know nothing about React my first approach was to just Google how to make a component and after that it was almost plain HTML/JS and some little details about React.

After I finished the components using classes I switched to functions to be able to show and hide components and being able to use the modal more easily.

I started writing this post-mortem right when I noticed that my approach to handling the data would make it difficult to open the modal and show the candidate info along with the application.