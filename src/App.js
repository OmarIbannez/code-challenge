import React, {useState}  from 'react';
import candidates from './data/candidates.json';
import nimbleScore from './ui-kit/icons/svg/nimble_score.svg';
import notes from './ui-kit/icons/svg/note.svg';
import scoreCard from './ui-kit/icons/svg/scorecard.svg';
import plusSign from './ui-kit/icons/svg/plus_sign.svg';
import {Modal} from './ui-kit/Modal'

const App = () => <Candidates candidates={candidates.results} />;

export default App;


function Candidates({ candidates }) {
  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">CANDIDATE NAME</th>
          <th scope="col">CANDIDATE STATUS</th>
          <th scope="col">APPLICATIONS</th>
          <th scope="col">LAST ACTION</th>
          <th scope="col"></th>
          <th scope="col"></th>
          <th scope="col"></th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
      {candidates.map(candidate => (
        <Candidate key={candidate.id} candidate={candidate}/>
      ))}
      </tbody>
    </table>
  )
}

function Candidate ({ candidate }) {
  const [showApplications, setshowApplications] = useState(false);
  return (
    <>
      <tr>
        <td>{candidate.name}</td>
        <td>{candidate.applications[0].new_status.label}</td>
        <td>{candidate.applications.length}</td>
        <td>7 months ago</td>
        <td><img src={scoreCard} /> {candidate.scorecard_count}</td>
        <td><img src={nimbleScore} /> {candidate.profile.nimble_score}</td>
        <td><img src={notes} /> {candidate.note_count}</td>
        <td><img src={plusSign} onClick={() => setshowApplications(!showApplications) } /></td>
      </tr>
      { showApplications ? <Applications applications={candidate.applications} /> : null}
    </>
  );
}

function Applications ({ applications }) {return (
  <table>
    {applications.map(application => (
      <Application key={application.id} application={application} />
    ))}
  </table>
)}

function Application ({ application }) {
  const [isOpen, setisOpen] = useState(false);
  return (
    <>
      <tr onClick={() => setisOpen(!isOpen) }>
          <td>{application.role.title}</td>
          <td>{application.new_status.label}</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
      <Modal isOpen={isOpen} onClose={() => setisOpen(!isOpen)} children=<ApplicationDetail /> />
    </>
)}

function ApplicationDetail({ application }) {
  return (
    <div>Application Info</div>
  )
}